//
//  QuizzController.swift
//  prophete
//
//  Created by Gigimac on 08/05/2018.
//  Copyright © 2018 Gigimac. All rights reserved.
//

import UIKit

class QuizzController: UIViewController {

    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var questionText: UITextView!
    
    
    @IBOutlet weak var bouton1: MonBouton!
    @IBOutlet weak var bouton2: MonBouton!
    @IBOutlet weak var bouton3: MonBouton!
    @IBOutlet weak var bouton4: MonBouton!
    
    
    @IBOutlet weak var blur: UIVisualEffectView!
    
    @IBOutlet weak var popup: MaVue!
    @IBOutlet weak var popImageView: UIImageView!
    
    @IBOutlet weak var popResultat: UILabel!
    @IBOutlet weak var popReponse: UILabel!
    
   
    
    let image = #imageLiteral(resourceName: "prophète")
    let tableauProphete = ["Daniel", "Jérémie", "Ézéchiel", "Obadia", "Habaquouq", "Nahoum","Tsephania", "Mika", "Isaïe", "Hoshéa", "Yoël", "Jonas", "Amos", "Éliya", "Élisha"  ]
    let DicoQuestions = ["Quel prophète de Jéhovah vécu d'environ  de 630 à 530 avant notre ére ?":"Daniel", "Quel prophète de Jéhovah fut déporté à Babylone en 617 avant notre ére ?": "Daniel", "Quel prophète de Jéhovah fut établi au dessus de 120 satrapes ?":"Daniel", "Quel prophète de Jéhovah vécu d'environ 650 à 580 avant notre ére ?": "Jérémie", "Quel prophète de Jéhovah fut fils d'un prêtre nommé Bouzi":"Ézéchiel", "Quel prophète de Jéhovah fut emmené captif en 617 av.n.è comme Daniel":"Ézéchiel", "Quel prophète de Jéhovah commença a prophétisé à 30 ans  alors qu'il était à Babylone?" :"Ézéchiel", "Quel prophète de Jéhovah était un lévite musicien au temple?":"Habaquouq", " Quel prophète de Jéhovah était appelé l'Elqoshite (habitant d'Elqosh)?" : "Nahoum" , "Quel prophète de Jéhovah à vécut au IXe siècle av. n. è et à un livre de la bible à son nom ?" : "Amos", "Quel prophète de Jéhovah était éleveur de mouton et pinceur de figues de sycomores ?":"Amos", "prophète de Jéhovah dont le nom signifie : Colombe":"Jonas", "Son nom signifie: Dieu est salut":"Élisha", "Quel prophète de Jéhovah fut le successeur d'Éliya ?":"Élisha", "Quel prophète de Jéhovah guérit la réserve d’eau de la ville de Jéricho ? ":"Élisha" ]
    
    var tableauQuestions2 = ["Quel prophète de Jéhovah vécu d'environ  de 630 à 530 avant notre ére ?","Quel prophète de Jéhovah fut déporté à Babylone en 617 avant notre ére ?", "Quel prophète de Jéhovah fut établi au dessus de 120 satrapes ?", "Quel prophète de Jéhovah vécu d'environ 650 à 580 avant notre ére ?","Quel prophète de Jéhovah fut fils d'un prêtre nommé Bouzi","Quel prophète de Jéhovah fut emmené captif en 617 av.n.è comme Daniel", "Quel prophète de Jéhovah commença a prophétisé à 30 ans  alors qu'il était à Babylone?","Quel prophète de Jéhovah était un lévite musicien au temple?", " Quel prophète de Jéhovah était appelé l'Elqoshite (habitant d'Elqosh)?", "Quel prophète de Jéhovah à vécut au IXe siècle av. n. è et à un livre de la bible à son nom ?", "Quel prophète de Jéhovah était éleveur de mouton et pinceur de figues de sycomores ?", "prophète de Jéhovah dont le nom signifie : Colombe", "Son nom signifie: Dieu est salut", "Quel prophète de Jéhovah fut le successeur d'Éliya ?", "Quel prophète de Jéhovah guérit la réserve d’eau de la ville de Jéricho ? "]
    
    var questions = [Question]()
    var questionPosee: Question?
    
    var questionActuelle = 1
    var score = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        questions = questionnaire()
        obtenirQuestion()
        print(questions)
        
    }
    
    func prophete() ->String {
        let choixreponse: String = tableauProphete[Int(arc4random_uniform(UInt32(tableauProphete.count)))]
        
        return choixreponse
    }
    
    func questionnaire() ->[Question]{
        var mesQuestions = [Question]()
        
        let portee = 1...11
        
        for _ in portee{
            
            let numeroQuestion = Int(arc4random_uniform(UInt32(tableauQuestions2.count)))
            let questionAleaoire = tableauQuestions2[numeroQuestion]
            
            tableauQuestions2.remove(at: numeroQuestion)
           
            
            
            func bonneReponse() -> String{
                let bonnereponse : String
               
                if  DicoQuestions[questionAleaoire] != nil {
                    
                bonnereponse = DicoQuestions[questionAleaoire]!
                
                }else{
                    bonnereponse = "ouille"
                }
                return bonnereponse
            }
         
            let reponse = bonneReponse()
            let aleatoire2 = Int(arc4random_uniform(UInt32(4))) + 1
            print(aleatoire2)
            
            var tableau4reponse = [String]()
            
            repeat {
            switch aleatoire2 {
            case 1:
                tableau4reponse = [reponse,prophete(),prophete(),prophete()]
            case 2:
                tableau4reponse = [prophete(),reponse,prophete(),prophete()]
            case 3:
                tableau4reponse = [prophete(),prophete(),reponse, prophete()]
            case 4:
                tableau4reponse = [prophete(),prophete(),prophete(), reponse]
                
            default: print("coucou")
                
            }
                
            } while tableau4reponse[0] == tableau4reponse[1] || tableau4reponse[0] == tableau4reponse[2] || tableau4reponse[0] == tableau4reponse[3] || tableau4reponse[1] == tableau4reponse[2] || tableau4reponse[1] == tableau4reponse[3] || tableau4reponse[2] == tableau4reponse[3]
        
            mesQuestions.append(Question(quest: questionAleaoire, rep1: tableau4reponse[0], rep2: tableau4reponse[1], rep3: tableau4reponse[2], rep4: tableau4reponse[3], tag: aleatoire2))
            
        }
        
        return mesQuestions
    }
    
    func obtenirQuestion() {
        
        if questionActuelle < 11 {
            
            scoreLabel.miseAJour(questionActuelle, score)
            questionPosee = questions[questionActuelle ]
            questionText.text = questions[questionActuelle ].QuestionAPoser
            bouton1.setTitle(questions[questionActuelle].reponse1, for: .normal)
            bouton2.setTitle(questions[questionActuelle].reponse2, for: .normal)
            bouton3.setTitle(questions[questionActuelle].reponse3, for: .normal)
            bouton4.setTitle(questions[questionActuelle].reponse4, for: .normal)
            questionActuelle += 1
            
        }else {
            let meilleurScore = UserDefaults.standard.integer(forKey: "score")
            if score > meilleurScore {
                
                UserDefaults.standard.set(score, forKey: "score")
                UserDefaults.standard.synchronize()
            }
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    func montrerPopUp(gagne: Bool, nom: String, image: UIImage) {
        popImageView.image = image
        popReponse.text = "La bonne réponse était: " + nom
        if gagne {
            popResultat.text = "Félicitations"
        } else {
            popResultat.text = "Mauvaise réponse"
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.blur.alpha = 1
            self.popup.alpha = 1
        }) { (success) in
            //
        }
    }
    
    @IBAction func popBoutonAppuye(_ sender: Any) {
        UIView.animate(withDuration: 0.3, animations: {
            self.blur.alpha = 0
            self.popup.alpha = 0
        }) { (success) in
            self.obtenirQuestion()
        }
        
        
    }
    @IBAction func boutonappuye(_ sender: Any) {
        if let question = questionPosee, let bouton = sender as? UIButton {
            if question.tagCorrect == bouton.tag {
                score += 1
                montrerPopUp(gagne: true, nom: bouton.titleLabel?.text ?? "", image: image)
            } else {
                var nom: String?
                switch question.tagCorrect {
                case 1: nom = bouton1.titleLabel?.text
                case 2: nom = bouton2.titleLabel?.text
                case 3: nom = bouton3.titleLabel?.text
                case 4: nom = bouton4.titleLabel?.text
                default: break
                }
                montrerPopUp(gagne: false, nom: nom ?? "", image: image)
            }
        } else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    



}

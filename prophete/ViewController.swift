//
//  ViewController.swift
//  prophete
//
//  Created by Gigimac on 08/05/2018.
//  Copyright © 2018 Gigimac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var meilleurScoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Quizz prophète de Jéhovah"
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let meilleurScore = UserDefaults.standard.integer(forKey: "score")
        let scoreString = "Meilleur score: \(meilleurScore)/10"
        meilleurScoreLabel.text = scoreString
    }
   


}

